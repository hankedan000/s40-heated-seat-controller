/*
 * seats.h
 *
 * Created: 11/19/2015 7:42:04 PM
 *  Author: Daniel
 */ 


#ifndef SEATS_H_
#define SEATS_H_

/* INDICATORS */
#define IND_TRIS		DDRB
#define IND_PORT		PORTB
#define IND_PIN			PINB
#define DHI_IND			PORTB0
#define DLO_IND			PORTB1
#define PHI_IND			PORTB2
#define PLO_IND			PORTB3
/* BUTTON */
#define BUTTON_TRIS		DDRB
#define BUTTON_PORT		PORTB
#define BUTTON_PIN		PINB
#define BUTTON			PORTB4
/* FET DRIVERS */
#define FET_TRIS		DDRA
#define FET_PORT		PORTA
#define FET_PIN			PINA
#define DLO_FET			PORTA1
#define DHI_FET			PORTA2
#define PHI_FET			PORTA3
#define PLO_FET			PORTA4

/* I/O MANIPULATORS */
#define PHI				IND_PORT |= (1<<PHI_IND); IND_PORT &= ~(1<<PLO_IND); FET_PORT |= (1<<PHI_FET); FET_PORT &= ~(1<<PLO_FET)
#define PLOW			IND_PORT |= (1<<PLO_IND); IND_PORT &= ~(1<<PHI_IND); FET_PORT &= ~(1<<PHI_FET); FET_PORT |= (1<<PLO_FET)
#define POFF			IND_PORT &= ~(1<<PLO_IND); IND_PORT &= ~(1<<PHI_IND); FET_PORT &= ~(1<<PHI_FET); FET_PORT &= ~(1<<PLO_FET)
#define DHI				IND_PORT |= (1<<DHI_IND); IND_PORT &= ~(1<<DLO_IND); FET_PORT |= (1<<DHI_FET); FET_PORT &= ~(1<<DLO_FET)
#define DLOW			IND_PORT |= (1<<DLO_IND); IND_PORT &= ~(1<<DHI_IND); FET_PORT &= ~(1<<DHI_FET); FET_PORT |= (1<<DLO_FET)
#define DOFF			IND_PORT &= ~(1<<DLO_IND); IND_PORT &= ~(1<<DHI_IND); FET_PORT &= ~(1<<DHI_FET); FET_PORT &= ~(1<<DLO_FET)
#define BUTTON_VAL		!!(BUTTON_PIN&(1<<BUTTON))

//--------------- STATE MACHINE STATES -----------------+
// Definition of state machine integers					|
//------------------------------------------------------+
#define OFF		0
#define STANDBY	1
#define PASS_HI	2
#define PASS_LO	3
#define DRIV_HI	4
#define DRIV_LO	5
#define BOTH_HI	6
#define BOTH_LO	7

//---------------- FUNCTION PROTOTYPES -----------------+
// Function prototypes for main							|
//------------------------------------------------------+
void init(void);


#endif /* SEATS_H_ */