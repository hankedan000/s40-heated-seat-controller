/*
 * s40_heated_seats.c
 *
 * Created: 11/17/2015 9:12:42 PM
 * Author : Daniel
 */ 
#include <avr/io.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "seats.h"

volatile uint8_t prev_state, pres_state, next_state;// stores state
volatile uint8_t interrupted;

//------------------------ MAIN ------------------------+
//														|
//------------------------------------------------------+
int main(void) {
	/* SETUP */
	init();
	
	/* PRE LAUNCH */
	_delay_ms(1000);// wait for power to stabilize
	pres_state = eeprom_read_byte((uint8_t*)0);// restore state code from previous power cycle

	while(1){
		interrupted = 0;// clear the interrupted flag
		
		switch(pres_state){
		case OFF:
			DOFF;	POFF;
			next_state = DRIV_HI;
			break;
			
		case DRIV_HI:
			DHI;	POFF;
			next_state = DRIV_LO;
			break;
			
		case DRIV_LO:
			DLOW;	POFF;
			next_state = BOTH_HI;
			break;
			
		case BOTH_HI:
			DHI;	PHI;
			next_state = BOTH_LO;
			break;
			
		case BOTH_LO:
			DLOW;	PLOW;
			next_state = PASS_HI;
			break;
			
		case PASS_HI:
			DOFF;	PHI;
			next_state = PASS_LO;
			break;
			
		case PASS_LO:
			DOFF;	PLOW;
			next_state = OFF;
			break;
			
		case STANDBY:
			DOFF;	POFF;
			next_state = prev_state;
			
			/* SPIN WHEELS WHILE USER HOLDS BUTTON */
			while(!BUTTON_VAL);
			
			/* DEBOUNCE */
			_delay_ms(100);
			break;
			
		default:
			next_state = OFF;
		}// switch
		
		//----------------------------------------------------------//
																	//
		/* WAIT FOR STATE CHANGE (BUTTON TO BE PRESSED) */			//
		while(BUTTON_VAL);											//
																	//
		/* START "PRESS AND HOLD" TIMER */							//
		TCNT0H = 0x00;												//		BLOCKING ZONE
		TCNT0L = 0x00;												//
		TIFR |= (1<<OCF0A);// clear interrupt flag					//
		if(pres_state!=STANDBY)										//
			TIMSK = (1<<OCIE0A);// enable timer0 interrupt			//
																	//
		/* SPIN WHEELS WHILE USER HOLDS BUTTON */					//
		while(!BUTTON_VAL && !interrupted);							//
																	//
		/* STOP TIMER */											//
		TIMSK &= ~(1<<OCIE0A);// disable timer0 interrupt			//
																	//
		//----------------------------------------------------------//
		
		prev_state = pres_state;
		pres_state = next_state;// increment state
		eeprom_write_byte((uint8_t*)0, pres_state);// store the new state
			
		/* DEBOUNCE */
		_delay_ms(100);
	}// while
	return 0;
}// end main()

//------------------------ INIT ------------------------+
// Initialize chips peripherals and I/O					|
//------------------------------------------------------+
void init(void){
	/* PORT SETUP */
	IND_TRIS |= (1<<PHI_IND)|(1<<PLO_IND)|(1<<DHI_IND)|(1<<DLO_IND);// OUTPUTS
	FET_TRIS |= (1<<PHI_FET)|(1<<PLO_FET)|(1<<DHI_FET)|(1<<DLO_FET);// OUTPUTS
	BUTTON_TRIS &= ~(1<<BUTTON);// INPUTS
	BUTTON_PORT |= (1<<BUTTON);// enable pull-ups on button
	
	/* TURN HEATERS OFF */
	DOFF;
	POFF;
	
	/* TIMER SETUP */
	TCCR0A = (1<<TCW0);// set timer0 to a 16bit counter
	TCCR0B = 0x4;// clock = Fcpu/256 = 8000000/256 = 31250Hz
	OCR0B = 0x7A;
	OCR0A = 0x12;// 16bit output compare value to 0x7A12
	sei();// enable global interrupts
}// end init()

//--------------------- TIMER0 ISR ---------------------+
// Used to time user input events						|
//------------------------------------------------------+
ISR(TIMER0_COMPA_vect){
	interrupted = 1;// set flag to trigger state change
	next_state = STANDBY;
}

